(function ($) {
  'use strict';
  Drupal.behaviors.manual = {
    handleManualDropdown: function () {
      var $manualItem = $('.manual--item');
      $manualItem.each(function(){
        var $this = $(this);
        var $manualItemTitle = $this.find('> h2');
        var $manualItemContent = $this.find('> .content');
        $manualItemTitle.click(function(){
          $manualItemTitle.toggleClass('open');
          $manualItemContent.slideToggle(200);
        });
      });
    },
    load: function () {
      this.handleManualDropdown();
    },
    attach: function (context) {

    }
  };

  $(document).ready(function(){
    Drupal.behaviors.manual.load();
  });

})(jQuery);
